import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  RouteProps
} from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";

import Books from "./views/BookView";

import { Button, Navbar, Alignment } from "@blueprintjs/core";

const App = () => {
  return (
    <Router>
      <div>
        <Navbar className="bp3-dark">
          <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>Book of the Quarter</Navbar.Heading>
            <Navbar.Divider />
            <Link to="/">
              <Button className="bp3-minimal" icon="home" text="Home" />
            </Link>
            <Link to="/books">
              <Button className="bp3-minimal" icon="book" text="Books" />
            </Link>
          </Navbar.Group>
        </Navbar>

        <Switch>
          <RouteView path="/books">
            <Books />
          </RouteView>
          <RouteView path="/users">{/* <Users /> */}</RouteView>
          <RouteView path="/">
            <Home />
          </RouteView>
        </Switch>
      </div>
    </Router>
  );
};

const RouteView: React.FC<RouteProps> = ({ children, ...props }) => {
  const routeViewStyle = { padding: 10 };

  return (
    <Route {...props}>
      <div style={routeViewStyle}>{children}</div>
    </Route>
  );
};

const Home = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
};

export default App;
