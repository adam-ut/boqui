import React from 'react';
import { Tag, Colors } from "@blueprintjs/core";

const Card = props => {
    function getField(filed){
        return props.book[filed] 
    }
    
    function signedUpForQuarter(quarter){
        const users = getField("users") || []
        return users.filter(function (user) {
            return user.quarter === quarter;
          }).length
    }
    function possibleFormat(format){
        const formats = getField("formats") || [] 
        return formats.filter(function (frmt){
            return frmt.format === format
        }).length === 1
    }
    
    function createTag(tag){
        return <Tag style={{ background: Colors.BLUE3, color: Colors.WHITE }}>{tag}</Tag> 
    }
    
    function getTags(){
        const tags = getField("tags") || []
        return tags.map(createTag)
    }
    
    function getAuthors(){
        let authors = getField("authors") || []
        const maxAuthors = 2
        if(authors.length > maxAuthors){
            authors[maxAuthors] = "et. al."
            return authors.slice(0, 3)
        } 
        return authors
    }
    
    const formatStyle = {
        display: "flex",
        "justify-content": "space-between",
        width: 250
    }
    
    const tagStyle = {
        display: "flex",
        "justify-content": "space-between",
        width: 500 
    }

    return (
        <div>
            {getField("title")} ({getField("publishYear")})<br/>
            {getAuthors().join(", ")}  ({getField("pages")} pages)<br/>
            {signedUpForQuarter("1Q2020")} users signed up for 1Q2020<br/>
            <div style={formatStyle}>
                <Tag minimal={! possibleFormat("Hardcover")}>Book</Tag>
                <Tag minimal={! possibleFormat("Audible")}>Audible</Tag>
                <Tag minimal={! possibleFormat("Kindle")}>Kindle</Tag>
                <Tag minimal={! possibleFormat("Safari")}>Safari</Tag>
            </div>
                <div style={tagStyle}>{getTags()}</div>
        </div>
    )
}

export default Card