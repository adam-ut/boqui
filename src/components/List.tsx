import * as React from "react";

interface Item {
  key: string;
}

interface ListProps {
  items: Array<Item>;
  itemRenderer: (item: any) => JSX.Element[] | JSX.Element;
}

interface ListItemProps {
  children: JSX.Element[] | JSX.Element;
}

const ListItem: React.FC<ListItemProps> = ({ children }) => {
  return <div className="list-item">{children}</div>;
};

const List: React.FC<ListProps> = ({ itemRenderer, items }) => {
  return (
    <div>
      {items.map(item => (
        <ListItem key={item.key}>{itemRenderer(item)}</ListItem>
      ))}
    </div>
  );
};

export default List;
