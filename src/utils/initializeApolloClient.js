import ApolloClient from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";

export default function init() {
  const httpLink = createHttpLink({
    uri:
      "https://qcz56yyeo5h55bzwy4rcnawcuy.appsync-api.us-east-2.amazonaws.com/graphql"
  });

  let apiKey = process.env.REACT_APP_APOLLO_TOKEN;

  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    //   const token = localStorage.getItem("token");
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        //   authorization: token ? `Bearer ${token}` : "",
        "x-api-key": apiKey
      }
    };
  });

  return new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  });
}
