import * as React from "react";

import List from "components/List";

import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Spinner, Intent } from "@blueprintjs/core";
import Card from "components/Card";

interface BookViewProps {}

interface IUser {
  username: string;
  name: string;
  quarter: string;
  priority: number;
  willLead: boolean;
  location: string;
  format: string;
}

interface IFormat {
  format: string;
  link: string;
}

interface IBook {
  title: string;
  description: string;
  authors: Array<string>;
  publishYear: number;
  tags: Array<string>;
  formats: Array<IFormat>;
  users: Array<IUser>;
  pages: number;
}
interface IBookListProps {
  books: Array<IBook>;
}

const BOOKS_QUERY = gql`
  {
    books(query: "") {
      id
      title
      description
      authors
      tags
      publishYear
      pages
      users{quarter}
    }
  }
`;

let bookCardStyle = {
  background: "palevioletred",
  height: 100,
  padding: 5,
  margin: 5,
  display: "flex",
  "flex-direction": "column"
};

const bookItemRenderer = (book: IBook) => (
  <Card book={book} />
);

const BookList: React.FC<IBookListProps> = ({ books }) => {
  let bookItems = books.map((book, index) => {
    return { ...book, key: book.title + index };
  });
  return <List items={bookItems} itemRenderer={bookItemRenderer} />;
};

const View: React.FC<BookViewProps> = props => {
  const { loading, data } = useQuery(BOOKS_QUERY);
  let Component;

  if (loading) {
    Component = <Spinner intent={Intent.PRIMARY} />;
  } else {
    let books: Array<IBook> = data.books;
    Component = <BookList books={books} />;
  }

  let containerStyles = {
    minHeight: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  };

  return (
    <div>
      <h1 className="bp3-heading">Books!</h1>
      <div style={containerStyles}>{Component}</div>
    </div>
  );
};

export default View;
